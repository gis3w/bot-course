# BOT COURSE #

I simple example use of GeoPandas Pandas and Python3 Asyncio module for
ARPA Piemonte(Italy)

## Resources ##

### Tutorials and introductions ##

#### Pandas ####
+ https://towardsdatascience.com/a-quick-introduction-to-the-pandas-python-library-f1b678f34673
+ https://medium.com/dunder-data/selecting-subsets-of-data-in-pandas-6fcd0170be9c
#### GeoPandas ####
+ https://geohackweek.github.io/vector/04-geopandas-intro/

#### AsyncIO ####
+ https://realpython.com/async-io-python/
+ https://hackernoon.com/a-simple-introduction-to-pythons-asyncio-595d9c9ecf8c