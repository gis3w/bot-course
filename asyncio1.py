# coding=utf-8
""" Sample use of AsyncIO
.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the Mozilla Public License 2.0.

    code from: https://realpython.com/async-io-python/ adapted to Python 3.6
"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2019-06-04'
__copyright__ = 'Copyright 2019, GIS3W'


import asyncio


async def count():
    print("One")
    await asyncio.sleep(1)
    print("Two")


async def main():
    await asyncio.gather(count(), count(), count())

if __name__ == "__main__":
    import time
    s = time.perf_counter()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")
