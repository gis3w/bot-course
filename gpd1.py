# coding=utf-8
""" Sample use of geopandas
.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the Mozilla Public License 2.0.
"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2019-06-03'
__copyright__ = 'Copyright 2019, GIS3W'

import matplotlib.pyplot as plt
import geopandas


file_data = 'data/comuni.geojson'

# instance and create a GeoDataFrame(gdf) froma GeoJSON file
gpd = geopandas.read_file(file_data)

# get columns name
print(list(gpd.columns))

# extract a sud df form main gdf and save it to a CSV file.
#gpd[['comune', 'provincia', 'targa', 'regione', 'totale', 'com_pre', 'principali', 'tot_esiti', 'tot_succe', 'a', 'af', 'b', 'bf', 'c', 'cf', 'd', 'df', 'e', 'ef', 'f', 'nd']].to_csv('data/comuni.csv')




# extraxt sub df where tot_esiti >= global tot_esiti_mean
# and plot in in a BarPlot
esiti_mean = gpd['tot_esiti'].mean()

# above esiti mean
aem = gpd[gpd['tot_esiti'] > esiti_mean][['comune', 'tot_esiti']]

aem.set_index('comune', inplace=True)
#aem.plot(kind='bar')

plt.show()
