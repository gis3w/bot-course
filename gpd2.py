# coding=utf-8
""" Sample use of geopandas
.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the Mozilla Public License 2.0.
"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2019-06-03'
__copyright__ = 'Copyright 2019, GIS3W'

import matplotlib.pyplot as plt
import geopandas

# import geopandas native resoruce naturalearth
gpd = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))

# get first 2 row
print(gpd.head(2))

# get columns name
print(list(gpd.columns))

# get pop_est
print(gpd.pop_est)

# plot data
ax = gpd.plot(facecolor='lightgray', figsize=(12, 6))

#plt.show()

# df countries with pop_est >= world mean population

gpd_abovemp = gpd[gpd['name'] == 'Italy']

# plot 2 df
gpd_abovemp.plot(ax=ax, cmap='Paired')

plt.show()



