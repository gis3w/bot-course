# coding=utf-8
""" Sample use of geopandas
.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the Mozilla Public License 2.0.
"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2019-06-03'
__copyright__ = 'Copyright 2019, GIS3W'

import matplotlib.pyplot as plt
import pandas


file_data = 'data/comuni.csv'

# instance and create a DataFrame(df)
pd = pandas.read_csv(file_data)

print(pd)

# get columns name
print(list(pd.columns))

subpd = pd.loc[pd.comune.str.lower() == 'viterbo']




# filter df by column comune and sum tot_esiti
pd.loc[pd.comune.str.lower() == 'viterbo']['tot_esiti'].sum()

# extract sub df from main df
esiti = pd[['comune', 'tot_esiti']]
esiti.set_index('comune', inplace=True)
print(esiti.head())

# plot sub df
#esiti.plot(kind='bar', y='tot_esiti')
#plt.show()

# extract sub df with tot_esiti > tot_esiti global mean
esiti_mean = pd['tot_esiti'].mean()
# above esiti mean
aem = pd[pd['tot_esiti'] > esiti_mean][['comune', 'tot_esiti']]
aem.set_index('comune', inplace=True)
aem.plot(kind='bar')

print(len(aem.index))

plt.show()
